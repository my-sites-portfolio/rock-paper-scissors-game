const buttons = document.querySelectorAll
('button');
const resultEl = document.getElementById('result');
const userScoreEl = document.getElementById('user-score');
const computerScoreEl = document.getElementById('computer-score');

let scores = {user: 0, computer: 0};


buttons.forEach(button => {
    button.addEventListener('click', () => {
        playRound(button.id, computerPlay());
    });
});


function computerPlay() {
    const choices = ['rock', 'paper', 'scissors'];
    const randomChoice = Math.
    floor(Math.random() * choices.length);
    return choices[randomChoice];
}

function playRound(playerSelection, computerSelection) {
    if (playerSelection === computerSelection) {
        resultEl.textContent = "It's a tie!";
    } else if (
        (playerSelection === 'rock' && computerSelection === 'paper') ||
        (playerSelection === 'paper' && computerSelection === 'scissors') ||
        (playerSelection === 'scissors' && computerSelection === 'rock') ) {
        resultEl.textContent = 'You lose! '+computerSelection+' beats '+playerSelection;
        computerScoreEl.textContent = ++scores.computer;
    } else {
        resultEl.textContent = "You win! " +playerSelection+" beats "+computerSelection;
        userScoreEl.textContent = ++scores.user;
    }
}